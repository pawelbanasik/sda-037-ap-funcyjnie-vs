package com.pawelbanasik;

public class FunctionExample {

	public static void main(String[] args) {
		// Function kiedy zwracam liczbe, consumer kiedy voida, i jest jeszcze
		// predykat
		System.out.println("sum(" + 5 + ")=" + sum(5, FunctionExample::generateSeries));
		System.out.println("sum(" + 5 + ")=" + sum(5, (i) -> 2 * i + 5));
		System.out.println("sum(" + 5 + ")=" + sum(5, (i) -> 5));
	}

	// podmienil sobie
	// public static int sum(int n, Function<Integer, Integer> series) {
	public static int sum(int n, SeriesGenerator series) {
		int result = 0;
		for (int i = 0; i < n; i++) {

			result += series.generate(i);
		}
		return result;
	}

	public static int generateSeries(int i) {

		return 2 * i + 5;
	}

}